# MiniTest

A minimal and portable unit test framework for C.

All you need to do is to add two files to your project: `minitest.c` and `minitest.h`.

## Example usage

### Unit test

```c
#include "fact.h"
#include "minitest.h"

TEST_MAIN("My factorial implementation") {
  TEST_CASE("Factorials of positive numbers") {
    CHECK_EQ_INT(fact(1), 1);
    CHECK_EQ_INT(fact(2), 2);
    CHECK_EQ_INT(fact(3), 6);
    CHECK_EQ_INT(fact(4), 24);
    CHECK_EQ_INT(fact(5), 119);  /* <- Should fail! */
  }
}
```

```bash
$ gcc -o fact_test fact_test.c fact.c minitest.c && ./fact_test
======================================================================
Testing My factorial implementation
----------------------------------------------------------------------
Test case: Factorials of positive numbers
......................................................................
fact_test.c:10: FAILED
  Expected: 119
    Actual: 120
----------------------------------------------------------------------
Result: 4 of 5 checks passed -> FAIL
======================================================================
```

### Benchmark

```c
#include "my_unit.h"
#include "minitest.h"

BENCH_MAIN("My unit") {
  BENCH_CASE("A slow function") {
    BENCH_LOOP_BEGIN(100);
    slow_function();
    BENCH_LOOP_END();
  }

  BENCH_CASE("A fast function") {
    BENCH_LOOP_BEGIN(10000);
    fast_function();
    BENCH_LOOP_END();
  }
}
```

```bash
$ gcc -o my_unit_bench my_unit_bench.c my_unit.c minitest.c && ./my_unit_bench
======================================================================
Benchmarking My unit
----------------------------------------------------------------------
A slow function: 502.6 µs
A fast function: 5.0 µs
======================================================================
```

