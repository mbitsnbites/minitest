// -*- mode: c; tab-width: 2; indent-tabs-mode: nil; -*-
//------------------------------------------------------------------------------
// This is free and unencumbered software released into the public domain.
//
// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a compiled
// binary, for any purpose, commercial or non-commercial, and by any
// means.
//
// In jurisdictions that recognize copyright laws, the author or authors
// of this software dedicate any and all copyright interest in the
// software to the public domain. We make this dedication for the benefit
// of the public at large and to the detriment of our heirs and
// successors. We intend this dedication to be an overt act of
// relinquishment in perpetuity of all present and future rights to this
// software under copyright law.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// For more information, please refer to <https://unlicense.org/>
//------------------------------------------------------------------------------

#include "minitest.h"

#include <inttypes.h>
#include <stdio.h>
#include <string.h>

// Platform dependent timing routines.
#if defined(_WIN32)
#undef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#undef NOMINMAX
#define NOMINMAX
#include <windows.h>
#define USE_WIN32PERFORMANCECOUNTER
#undef ERROR
#undef log
#else
#include <sys/time.h>
#define USE_GETTIMEOFDAY
#endif

//------------------------------------------------------------------------------
// Constants.
//------------------------------------------------------------------------------

static const char SEPARATOR1[] =
    "======================================================================\n";
static const char SEPARATOR2[] =
    "----------------------------------------------------------------------\n";
static const char SEPARATOR3[] =
    "......................................................................\n";

//------------------------------------------------------------------------------
// Static variables.
//------------------------------------------------------------------------------

static const char* s_current_test_case_name;
static int s_current_test_case_name_printed;
static int s_num_checks_passed;
static int s_num_checks_failed;

static const char* s_current_bench_name;
static uint64_t s_bench_outer_smallest_dt;
static uint64_t s_bench_inner_t0;
static uint64_t s_bench_inner_count;

//------------------------------------------------------------------------------
// Private helper functions.
//------------------------------------------------------------------------------

static uint64_t get_time_micros(void) {
#if defined(USE_GETTIMEOFDAY)
  // Get current time.
  struct timeval tv;
  (void)gettimeofday(&tv, NULL);

  // Convert to microseconds.
  return ((uint64_t)tv.tv_sec * (uint64_t)1000000U) + (uint64_t)tv.tv_usec;
#else
  LARGE_INTEGER l;

  // Get the clock frequency (only needed once).
  static int s_initialized = 0;
  static uint64_t s_freq;
  static uint64_t s_t0;
  if (!s_initialized) {
    QueryPerformanceFrequency(&l);
    s_freq = (uint64_t)l.QuadPart;
    QueryPerformanceCounter(&l);
    s_t0 = (uint64_t)l.QuadPart;
    s_initialized = 1;
  }

  // Get the tick count.
  QueryPerformanceCounter(&l);
  const uint64_t t = (uint64_t)l.QuadPart;

  // Convert to microseconds.
  // Note: We use a relative tick count to minimize the risk for overflows.
  return ((t - s_t0) * (int64_t)1000000U) / s_freq;
#endif
}

//------------------------------------------------------------------------------
// Public API.
//------------------------------------------------------------------------------

void minitest_test_start_(const char* name) {
  fprintf(stderr, SEPARATOR1);
  fprintf(stderr, "Testing %s\n", name);
  s_current_test_case_name = "";
  s_current_test_case_name_printed = 0;
  s_num_checks_passed = 0;
  s_num_checks_failed = 0;
}

int minitest_test_end_(void) {
  int num_checks = s_num_checks_passed + s_num_checks_failed;

  fprintf(stderr, SEPARATOR2);
  fprintf(stderr,
          "Result: %d of %d checks passed -> ",
          s_num_checks_passed,
          num_checks);
  if (s_num_checks_failed == 0) {
    fprintf(stderr, "ALL CHECKS PASSED!\n");
  } else {
    fprintf(stderr, "FAIL\n");
  }
  fprintf(stderr, SEPARATOR1);

  return s_num_checks_failed == 0 ? 0 : 1;
}

void minitest_test_case_(const char* name) {
  s_current_test_case_name = name;
  s_current_test_case_name_printed = 0;
}

void minitest_assert_failed_(const char* file_name, int line_no) {
  // Extract the file name part (skip full path).
#if defined(_WIN32)
  const char DIR_SEPARATOR = '\\';
#else
  const char DIR_SEPARATOR = '/';
#endif
  const char* last_slash = strrchr(file_name, DIR_SEPARATOR);
  if (last_slash != NULL) {
    file_name = last_slash + 1;
  }

  if (!s_current_test_case_name_printed) {
    fprintf(stderr, SEPARATOR2);
    fprintf(stderr, "Test case: %s\n", s_current_test_case_name);
    fprintf(stderr, SEPARATOR3);
    s_current_test_case_name_printed = 1;
  }

  fprintf(stderr, "%s:%d: FAILED\n", file_name, line_no);
  ++s_num_checks_failed;
}

void minitest_assert_passed_(void) {
  ++s_num_checks_passed;
}

void minitest_print_int_(const char* caption, int64_t value) {
  fprintf(stderr, "  %s: %" PRId64 "\n", caption, value);
}

void minitest_print_uint_(const char* caption, uint64_t value) {
  fprintf(stderr, "  %s: %" PRIu64 "\n", caption, value);
}

void minitest_print_uintx_(const char* caption, uint64_t value) {
  fprintf(stderr, "  %s: 0x%016" PRIx64 "\n", caption, value);
}

void minitest_print_bool_(const char* caption, bool value) {
  fprintf(stderr, "  %s: %s\n", caption, value ? "true" : "false");
}

void minitest_print_str_(const char* caption, const char* value) {
  fprintf(stderr, "  %s: \"%s\"\n", caption, value);
}

void minitest_print_buf_(const char* caption, const void* buf, size_t len) {
  fprintf(stderr, "  %s: ", caption);
  const uint8_t* ptr = (const uint8_t*)buf;
  for (size_t i = 0U; i < len; ++i) {
    unsigned value = ptr[i];
    fprintf(stderr, "%02x", value);
    if ((i + 1U) < len) {
      fprintf(stderr, " ");
    }
  }
  fprintf(stderr, "\n");
}

void minitest_bench_start_(const char* name) {
  fprintf(stderr, SEPARATOR1);
  fprintf(stderr, "Benchmarking %s\n", name);
  fprintf(stderr, SEPARATOR2);
}

int minitest_bench_end_(void) {
  // TODO: Print summary.
  fprintf(stderr, SEPARATOR1);

  return 0;
}

void minitest_bench_case_(const char* name) {
  s_current_bench_name = name;
}

void minitest_bench_outer_begin_(uint64_t inner_count) {
  s_bench_outer_smallest_dt = UINT64_MAX;
  s_bench_inner_count = inner_count;
}

void minitest_bench_outer_end_(void) {
  const double dt_per_run =
      ((double)s_bench_outer_smallest_dt) / (double)s_bench_inner_count;
  fprintf(stderr, "%s: %.1f µs\n", s_current_bench_name, dt_per_run);
}

void minitest_bench_inner_begin_(void) {
  s_bench_inner_t0 = get_time_micros();
}

void minitest_bench_inner_end_(void) {
  const uint64_t dt = get_time_micros() - s_bench_inner_t0;
  if (dt < s_bench_outer_smallest_dt) {
    s_bench_outer_smallest_dt = dt;
  }
}
