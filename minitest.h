// -*- mode: c; tab-width: 2; indent-tabs-mode: nil; -*-
//------------------------------------------------------------------------------
// This is free and unencumbered software released into the public domain.
//
// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a compiled
// binary, for any purpose, commercial or non-commercial, and by any
// means.
//
// In jurisdictions that recognize copyright laws, the author or authors
// of this software dedicate any and all copyright interest in the
// software to the public domain. We make this dedication for the benefit
// of the public at large and to the detriment of our heirs and
// successors. We intend this dedication to be an overt act of
// relinquishment in perpetuity of all present and future rights to this
// software under copyright law.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// For more information, please refer to <https://unlicense.org/>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Unit test macros and functions.
//------------------------------------------------------------------------------

#ifndef MINITEST_H_
#define MINITEST_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

//------------------------------------------------------------------------------
// Test definition macros.
//------------------------------------------------------------------------------

// This will be defined by TEST_MAIN.
void minitest_test_main_(void);

#define TEST_MAIN(name)          \
  int main(void) {               \
    minitest_test_start_(name);  \
    minitest_test_main_();       \
    return minitest_test_end_(); \
  }                              \
  void minitest_test_main_(void)

#define TEST_CASE(name) minitest_test_case_(name);

//------------------------------------------------------------------------------
// Assert and check macros.
//------------------------------------------------------------------------------

#define MINITEST_ASSERT_(x, print_expected, print_actual) \
  do {                                                    \
    if (!(x)) {                                           \
      minitest_assert_failed_(__FILE__, __LINE__);        \
      print_expected;                                     \
      print_actual;                                       \
    } else {                                              \
      minitest_assert_passed_();                          \
    }                                                     \
  } while (0);

#define CHECK(a) MINITEST_ASSERT_(a, void, void)

#define CHECK_EQ_INT(a, b)                                        \
  MINITEST_ASSERT_((a) == (b),                                    \
                   minitest_print_int_("Expected", (int64_t)(b)), \
                   minitest_print_int_("  Actual", (int64_t)(a)))

#define CHECK_EQ_UINT(a, b)                                         \
  MINITEST_ASSERT_((a) == (b),                                      \
                   minitest_print_uint_("Expected", (uint64_t)(b)), \
                   minitest_print_uint_("  Actual", (uint64_t)(a)))

#define CHECK_EQ_UINTX(a, b)                                         \
  MINITEST_ASSERT_((a) == (b),                                       \
                   minitest_print_uintx_("Expected", (uint64_t)(b)), \
                   minitest_print_uintx_("  Actual", (uint64_t)(a)))

#define CHECK_EQ_BOOL(a, b)                                     \
  MINITEST_ASSERT_((!!(a)) == (!!(b)),                          \
                   minitest_print_bool_("Expected", (bool)(b)), \
                   minitest_print_bool_("  Actual", (bool)(a)))

#define CHECK_EQ_STR(a, b)                               \
  MINITEST_ASSERT_(strcmp((a), (b)) == 0,                \
                   minitest_print_str_("Expected", (b)), \
                   minitest_print_str_("  Actual", (a)))

#define CHECK_EQ_BUF(a, b)                                          \
  MINITEST_ASSERT_(                                                 \
      memcmp((a), (b), sizeof(b)) == 0,                             \
      minitest_print_buf_("Expected", (const void*)(b), sizeof(b)), \
      minitest_print_buf_("  Actual", (const void*)(a), sizeof(b)))

//------------------------------------------------------------------------------
// Performance benchmarking macros.
//------------------------------------------------------------------------------

#define MINITEST_BENCH_OUTER_LOOPS_ 10

// This will be defined by BENCH_MAIN.
void minitest_bench_main_(void);

#define BENCH_MAIN(name)          \
  int main(void) {                \
    minitest_bench_start_(name);  \
    minitest_bench_main_();       \
    return minitest_bench_end_(); \
  }                               \
  void minitest_bench_main_(void)

#define BENCH_CASE(name) minitest_bench_case_(name);

#define BENCH_LOOP_BEGIN(iterations)                                           \
  minitest_bench_outer_begin_(iterations);                                     \
  for (int _outer_i = 0; _outer_i < MINITEST_BENCH_OUTER_LOOPS_; ++_outer_i) { \
    minitest_bench_inner_begin_();                                             \
    for (int _inner_i = 0; _inner_i < (iterations); ++_inner_i) {
#define BENCH_LOOP_END()       \
  }                            \
  minitest_bench_inner_end_(); \
  }                            \
  minitest_bench_outer_end_()

//------------------------------------------------------------------------------
// Helper functions that are implemented in minitest.c.
//------------------------------------------------------------------------------

void minitest_test_start_(const char* name);
int minitest_test_end_(void);
void minitest_test_case_(const char* name);

void minitest_assert_failed_(const char* file_name, int line_no);
void minitest_assert_passed_(void);

void minitest_print_int_(const char* caption, int64_t value);
void minitest_print_uint_(const char* caption, uint64_t value);
void minitest_print_uintx_(const char* caption, uint64_t value);
void minitest_print_bool_(const char* caption, bool value);
void minitest_print_str_(const char* caption, const char* value);
void minitest_print_buf_(const char* caption, const void* buf, size_t len);

void minitest_bench_start_(const char* name);
int minitest_bench_end_(void);
void minitest_bench_case_(const char* name);

void minitest_bench_outer_begin_(uint64_t inner_count);
void minitest_bench_outer_end_(void);
void minitest_bench_inner_begin_(void);
void minitest_bench_inner_end_(void);

#endif  // MINITEST_H_
